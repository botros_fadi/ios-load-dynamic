//
//  Loader.h
//  Dynamic
//
//  Created by fadi on 3/11/19.
//  Copyright © 2019 Experiments. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Loader : NSObject

-(int)callItWithParameter:(int) parameter other:(int) otherParameter;

@end

NS_ASSUME_NONNULL_END
