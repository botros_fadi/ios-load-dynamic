//
//  Loader.m
//  Dynamic
//
//  Created by fadi on 3/11/19.
//  Copyright © 2019 Experiments. All rights reserved.
//

#import "Loader.h"

@implementation Loader

-(int)callItWithParameter:(int) parameter other:(int) otherParameter {
    Class class = NSClassFromString(@"ADynamicObject");
    id object = [[class alloc] init];
    return [[object performSelector:NSSelectorFromString(@"somethingWithParameter:andNumber:") withObject:@(parameter) withObject:@(otherParameter)] integerValue];
}

@end
